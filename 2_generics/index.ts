function getString(params: string): string {
  return params
}

function getAny(params: any): any {
  return params
}

function getAll<T>(params: T): T {
  return params
}

// getAll('string')
// getAll<number>('string')

function getLegth<T extends { length: number }>(params: T): number {
  return params.length
}

getLegth('string') // 6
getLegth([]) // 0
// getLegth(5)

interface GenericI<T, K> {
  add: (item: T) => void
  getAll: () => T[]
  getOne: (item: K) => T
}

class CountryBox<V, K extends number> implements GenericI<V, K> {
  private list: V[] = []

  add(item: V): void {
    this.list.push(item)
  }

  getAll() {
    return this.list
  }

  getOne(index: K) {
    return this.list[index]
  }
}

enum Mynumber {
  zero,
  one,
  two
}

const countries: CountryBox<string, Mynumber> = new CountryBox()

countries.add('Kazan')
countries.add('Irkutsk')

console.log(countries.getAll())
console.log(countries.getOne(Mynumber.one))


/**
 * создание фабрики
 */
interface Ageable {
  getAge: () => number
}

abstract class Animal implements Ageable {
  protected age: number

  getAge() {
    return this.age
  }
}

class Dog extends Animal {
  age = 2
  say(): void {
    console.log('gaf')
  }
}

class Cat extends Animal {
  age = 1
  say(): void {
    console.log('may')
  }
}

function createInstance<T extends Animal>(c: new () => T): T {
  return new c()
}

const dog = createInstance(Dog)
dog.say()
console.log(dog.age)

const cat = createInstance(Cat)
cat.say()
console.log(cat.age)
