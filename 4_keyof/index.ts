/**
 * Оператор keyof принимает тип объекта и создает строковое или числовое буквальное объединение его ключей.
 */
type Point = {
  x: number,
  y: number
}
type P = keyof Point // значение может быть 'x' или 'y'
const p: P = 'y'

type Arrayish = { [n: number]: unknown }
const arrayish: Arrayish = {
  0: 1,
  1: 2
}

type AI = keyof Arrayish
const ai: AI = 15
