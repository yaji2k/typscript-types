/**
 * typeof в контексте типов. для формирования ссылки на тип переменной или свойства
 */

let s = 'hello'
// let ss:  typeof s = 5
let ss:  typeof s = 'ss'

function myFunc() {
  return { x: 1, y:1 }
}
type Obj = ReturnType<typeof myFunc>
const obj: Obj = { x:2, y:2 }
