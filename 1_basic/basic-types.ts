let unknownProp: unknown = 'string'
unknownProp.length

if(typeof unknownProp === 'string') {
  unknownProp.length
  unknownProp.push('s')
}

let anyProp: any = 'str'
anyProp.length
anyProp.push('s')

let nullProp: null = null
nullProp = undefined

 let undefinedProp: undefined = undefined
 undefinedProp = null

const getVoid = (): void => {
  console.log('s')
}

const getNever = (): never => {
  while(true) {
    console.log('s')
  }
}

let numberProp = 1

let bigintProp = 112n

let booleanProp = false

let stringProp = 'sdt'

let symbolProp: symbol = Symbol()

let objectProp: object = {}

let arrayProp: string[] = ['1']
let arrayProp1: Array<string | number> = ['1', 2]

let funcProp = () => {}
funcProp.call(this)

let tupleProp: [string, number] = ['1', 2]
tupleProp.push(1)
tupleProp = ['1', 2, 3]