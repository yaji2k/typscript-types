/**
 * 11/20
 */

type PersonExample = {
  name: string,
  age: number,
  hidden: boolean,
  desc?: string,
  readonly sex: string // я не гомофоб
  getSex: () => string 
}

/**
 * @name Partial
 * вернет тип с не обязательными полями
 */
type PersonPartial = Partial<PersonExample>
const personPartial: PersonPartial = {
  desc: 'sd' //  поле не обязательно
}

/**
 * @name Required
 * вернет тип с обязательными полями
 */
 type PersonRequired = Required<PersonExample>
 const personRequired: PersonRequired = {
   name: '',
   age: 0,
   hidden: false,
   sex: 'any',
   desc: 'gg',
   getSex: () => 'any'
 }

 /**
 * @name Readonly
 * вернет тип с полями только для чтения
 */
  type PersonReadonly = Readonly<PersonExample>
  const personReadonly: PersonReadonly = {
    name: '',
    age: 0,
    hidden: false,
    sex: 'any',
    desc: 'gg',
    getSex: () => 'any'
  }
  // personReadonly.name = '2' // поле менять нельзя

  /**
   * @name Record
   * вернет тип объекта ключи которого являются типы из Names
   * а типом этих ключей будет CatInfo
   */

  interface CatInfo{
    age: number
    color: string
  }
  type Names = 'flyffy' | 'miffy'
  const cats: Record<Names, CatInfo> = {
    flyffy: { age: 1, color: 'grey' },
    miffy: { age: 1, color: 'white' }
  }

  /** 
   * @name Pick
   * создаст тип из перечисленных полей, остальные отбросит
  */
  interface Todo {
    title: string
    description: string
    completed: boolean
  }
   
  type TodoPreview = Pick<Todo, 'title' | 'completed'>
   
  const todo: TodoPreview = {
    title: '',
    completed: false,
  }

  /**
   * @name Omit
   * создаст тип из всех полей исключив перечисленные
   */
  interface Article {
    title: string
    description: string
    completed: boolean
    createdAt: number
  }
   
  type ArticlePreview = Omit<Article, "description">;
   
  const article: ArticlePreview = {
    title: '',
    completed: false,
    createdAt: 1615544252770,
  }

  /**
   * @name Exclude
   * исключает тип из перечисленных типов
   */

  type EX0 = Exclude<string | number | boolean, boolean>
  let ex0: EX0 = ''
  ex0 = 3
  //ex0 = true // ошибка

  /**
   * @name Extract
   */
  type EXT0 = Extract<string | number | boolean, string>
  let ext0: EXT0 = ''
  //ext0 = 5
  
  /**
   * @name NonNullable
   * Создает тип, исключая значения null и undefined
   */

  type NonNullableType = NonNullable<string | number | undefined>
  const nonNullableType: NonNullableType = ''

  /**
   * @name Parameters
   * возвращает кортежный тип из аргументов функции
   */
  const tuple1: Parameters<(s: string, n: number) => {}> = ['1', 2]

  function f1(args: {s: string, n: number}): void {}
  const tuple2: Parameters<typeof f1> = [{ s: '', n: 3 }]

  /**
   * @name InstanceType
   * Создает тип, состоящий из типа экземпляра функции-конструктора в Type.
   */
  class C {
    x = 0
    y = 0
  }
   
  type T0 = InstanceType<typeof C>
  const t0: T0 = new C()

  type T1 = InstanceType<typeof Array>
  const t1: T1 = new Array()

  type T2 = InstanceType<any>
  const t2: T2 = new String()

  // type T3 = InstanceType<string>