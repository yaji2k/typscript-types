type Person = { name: string, age: number }

type Age = Person['age']
const age: Age = 3

const people = [
  { name: 'Bob', age: 30 },
  { name: 'Eva', age: 18 },
  // { name: 'Tom', weight: 48 }
]
type Person2 = typeof people[number]
const person: Person2 = { name: 'Eva', age: 18 }