type Person = {
  name: string,
  age: number
}
/**
 * Тип разрешающий поле с любым названием в которую можно передать или boolean или объект типа Person
 */
type User = {
  [key: string]: boolean | Person;
};
let user: User = {
}
user = {
  hidden: false,
  person: {
    name: '',
    age: 0
  }
}

/**
 * Создания типа на основе типа переденного в Generic
 */
type OptionsFlags<Type> = {
  [p in keyof Type]: Type[p]
};
const user2: OptionsFlags<Person> = {
  name: '',
  age: 2
}

/**
 * Создание типа с обязательными полями из типа с полями только для чтения и не обязательными полями
 */
type SuperUser = {
  readonly name: string,
  age?: number
}
type OptionSuper<T> = {
  -readonly[p in keyof T]-?: T[p]
}
let superUser: OptionSuper<SuperUser>
superUser = { name: '', age: 0 }
superUser.name = 'Tom'

/**
 * Тип геттеров
 */
type Getters<T> = {
  [p in keyof T as `get${Capitalize<string & p>}`]: () => T[p]
}
let gettersUser: Getters<Person> = {
  getAge: () => 1,
  getName: () => ''
}