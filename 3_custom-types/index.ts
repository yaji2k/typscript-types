/**
 * Тип при помощи интерфейса
 */
interface Foo {
  age: number
}

/**
 * Расширние существующего интерфейса
 */
interface Foo {
  name: string
}

const foo: Foo = { name: '', age: 0 }

/**
 * Создание типа при помощи алиаса
 */
type A = { a: string }
type B = { b: string }
type C = A & B
const c: C = { a: '', b: '' }

/** создание типа из типа и интерфейса */
const cAndFoo: C & Foo = { a: '', b: '', name: '', age: 0 }

type AOrB = A | B
let aOrB: AOrB = { b: '' }
aOrB = { a: '' }

class D implements A, B {
  a: string
  b: string
}
const d: D = { a: '' , b: '' }

/**
 * создание типа при помощи типа-литерала
 */
const key: 'key' = 'key'
type key = 'key'
const keyString: `${key}_${string}` = 'key_sadasd'