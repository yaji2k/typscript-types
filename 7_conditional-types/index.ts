interface IAge {
  age: number
}

interface IName {
  name: string
}

type AgeOrName<T extends number | string> = T extends number ? IAge : IName
const age2: AgeOrName<'ts'> = { name: 'string' }
